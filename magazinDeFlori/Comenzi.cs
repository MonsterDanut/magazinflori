﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace magazinDeFlori
{
    public partial class Comenzi : UserControl
    {
        int id;
        public Comenzi()
        {
            InitializeComponent();
            comboBox2.SelectedItem = "Activ";
        }

        private void Comenzi_Load(object sender, EventArgs e)
        {
            using (SqlConnection sqlConnection = new SqlConnection(Properties.Settings.Default.MagazinFloriConnectionString))
            {
                DataTable data = new DataTable();
                data.Clear();
                if (sqlConnection.State != ConnectionState.Open)
                {
                    sqlConnection.Open();
                }
                SqlDataAdapter sqlData = new SqlDataAdapter("Select Connections.Id_Comm As Id_Comanda, Comenzi.Nume_Client, Comenzi.Telefon, Comenzi.Data, Comenzi.Adresa, Comenzi.Statut, Produse.Nume_Produs, Produse.Pret, Connections.Cantitate, Connections.Cantitate*Produse.Pret AS Total From Comenzi, Connections, Produse Where Comenzi.Id_Comanda=Connections.Id_Comm and Connections.Id_Prodd=Produse.Id_Produs;", sqlConnection);
                sqlData.Fill(data);
                dataGridView1.DataSource = data;
                sqlConnection.Close();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count > 0)
            {
                int index = dataGridView1.SelectedRows[0].Index;
                bool converted = Int32.TryParse(dataGridView1[0, index].Value.ToString(), out id);
                if (converted == false)
                    return;
                button3.Visible = false;
                button4.Visible = true;
                comboBox2.Visible = true;
                button1.Visible = true;
                button2.Visible = false;
            }
            else
            {
                MessageBox.Show("Selectati randul statutul caruia doriti sa-l modificati!");
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (comboBox2.SelectedItem == "Activ" || comboBox2.SelectedItem == "Livrat" || comboBox2.SelectedItem == "Anulat")
            {
                DialogResult dialogResult = MessageBox.Show("Sunteti sigur ca doriti sa modificati statutul produsului selectat??", "Some Title", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    if (string.IsNullOrEmpty(comboBox2.Text))
                    {
                        MessageBox.Show("No Item is Selected");
                    }
                    SqlConnection conn = new SqlConnection(Properties.Settings.Default.MagazinFloriConnectionString);
                    conn.Open();
                    string sql = "Update Comenzi SET Statut='" + comboBox2.SelectedItem + "' Where Id_Comanda=" + id + ";";
                    SqlCommand command = new SqlCommand(sql, conn);
                    command.ExecuteNonQuery();
                    conn.Close();
                    MessageBox.Show("Produs trecut in statut: " + comboBox2.SelectedItem);
                    button3.Visible = true;
                    button4.Visible = false;
                    comboBox2.Visible = false;
                    button1.Visible = false;
                    button2.Visible = true;
                }
                else if (dialogResult == DialogResult.No)
                {
                    button3.Visible = true;
                    button4.Visible = false;
                    comboBox2.Visible = false;
                    button1.Visible = false;
                    button2.Visible = true;
                }
            }
            else
            {
                MessageBox.Show("Selectati unul din statutul propus");
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            button3.Visible = true;
            button4.Visible = false;
            comboBox2.Visible = false;
            button1.Visible = false;
            button2.Visible = true;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count > 0)
            {
                int index = dataGridView1.SelectedRows[0].Index;
                bool converted = Int32.TryParse(dataGridView1[0, index].Value.ToString(), out id);
                if (converted == false)
                    return;
                DialogResult dialogResult = MessageBox.Show("Sunteti sigur ca doriti sa stergeti aceasta comanda?", "Some Title", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    SqlConnection conn = new SqlConnection(Properties.Settings.Default.MagazinFloriConnectionString);
                    conn.Open();
                    string sql = "alter table Connections nocheck constraint all BEGIN TRANSACTION DELETE FROM Comenzi WHERE Id_Comanda = " + id + " DELETE FROM Connections Where Id_Comm= " + id + " COMMIT alter table Connections check constraint all";
                    SqlCommand command = new SqlCommand(sql, conn);
                    command.ExecuteNonQuery();
                    conn.Close();
                    MessageBox.Show("Comanda a fost stearsa cu succes!");
                }
                else if (dialogResult == DialogResult.No)
                {

                }
            }
            else
            {
                MessageBox.Show("Selectati randul statutul caruia doriti sa-l modificati!");
            }
        }
    }
}
