﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;
using System.Data.SqlClient;
using System.IO;

namespace magazinDeFlori
{
    public partial class AfisareProduse : UserControl
    {
        SqlConnection conn = new SqlConnection(Properties.Settings.Default.MagazinFloriConnectionString);
        private int numberProm = 0;
        public AfisareProduse()
        {
            InitializeComponent();
        }
        public void AfisareElemente(string categorie)
        {
            this.Controls.Clear();
            using (SqlConnection sqlConnection = new SqlConnection(Properties.Settings.Default.MagazinFloriConnectionString))
            {
                DataTable data = new DataTable();
                data.Clear();
                if (sqlConnection.State != ConnectionState.Open)
                {
                    sqlConnection.Open();
                }
                SqlDataAdapter sqlData = new SqlDataAdapter("Select Count(*) From Produse where Produse.Categorie='" + categorie + "'", sqlConnection);
                sqlData.Fill(data);
                numberProm = int.Parse(data.Rows[0][0].ToString());
                sqlConnection.Close();
            }
            using (SqlConnection sqlConnection = new SqlConnection(Properties.Settings.Default.MagazinFloriConnectionString))
            {
                DataTable data = new DataTable();
                data.Clear();
                if (sqlConnection.State != ConnectionState.Open)
                {
                    sqlConnection.Open();
                }
                SqlDataAdapter sqlData = new SqlDataAdapter("Select * From Produse where Produse.Categorie='" + categorie + "'", sqlConnection);
                sqlData.Fill(data);
                sqlConnection.Close();
                int itemRind = 0;
                int cresteY = 45;
                for (int i = 0; i < numberProm; i++)
                {
                    Panel panel = new Panel
                    {
                        BackColor = Color.FromArgb(255, 204, 179),
                        TabIndex = i,
                        Size = new Size(250, 350)
                    };
                    Label labelName = new Label
                    {
                        Location = new Point(10, 10),
                        AutoSize = false,
                        Size = new Size(230, 30),
                        Name = "labelName",
                        TabIndex = i,
                        Text = data.Rows[i][1].ToString(),
                        Font = new Font("Arial", 15, FontStyle.Bold)
                    };
                    MemoryStream ms = new MemoryStream((byte[])data.Rows[i][4]);
                    PictureBox imagineProdus = new PictureBox
                    {
                        Location = new Point(10, 50),
                        Size = new Size(230, 190),
                        Image = Image.FromStream(ms),
                        SizeMode = PictureBoxSizeMode.StretchImage
                    };
                    Label descriere = new Label
                    {
                        Location = new Point(10, 245),
                        AutoSize = false,
                        Size = new Size(230, 60),
                        Name = "descriere",
                        TabIndex = i,
                        Text = data.Rows[i][3].ToString(),
                        Font = new Font("Arial", 10)
                    };
                    Label pret = new Label
                    {
                        Location = new Point(10, 310),
                        AutoSize = false,
                        Size = new Size(110, 30),
                        Name = "pret",
                        TabIndex = i,
                        Text = data.Rows[i][2].ToString() + " Lei",
                        Font = new Font("Arial", 14, FontStyle.Bold),
                        ForeColor = Color.FromArgb(245, 138, 8)
                    };
                    Button addProdus = new Button
                    {
                        Location = new Point(120, 310),
                        Size = new Size(110, 30),
                        Name = labelName.Text,
                        TabIndex = i,
                        Text = "Comanda",
                        Font = new Font("Arial", 14),
                        BackColor = Color.FromArgb(245, 138, 8),
                        FlatStyle = FlatStyle.Flat
                    };
                    addProdus.FlatAppearance.BorderSize = 0;
                    addProdus.Click += new EventHandler(this.MyButtonHandler);
                    if (itemRind != 2)
                    {
                        if (i % 2 == 0)
                        {
                            panel.Location = new Point(80, cresteY);
                            itemRind++;
                        }
                        else
                        {
                            panel.Location = new Point(400, cresteY);
                            itemRind++;
                        }
                    }
                    else
                    {
                        itemRind = 1;
                        cresteY += 400;
                        panel.Location = new Point(80, cresteY);
                    }
                    this.Controls.Add(panel);
                    panel.Controls.Add(labelName);
                    panel.Controls.Add(imagineProdus);
                    panel.Controls.Add(descriere);
                    panel.Controls.Add(pret);
                    panel.Controls.Add(addProdus);
                }
            }
        }
        private void MyButtonHandler(object sender, EventArgs e)
        {
            int id = 0;
            string name = null;
            int price = 0;
            using (SqlConnection sqlConnection = new SqlConnection(Properties.Settings.Default.MagazinFloriConnectionString))
            {
                DataTable data = new DataTable();
                data.Clear();
                if (sqlConnection.State != ConnectionState.Open)
                {
                    sqlConnection.Open();
                }
                SqlDataAdapter sqlData = new SqlDataAdapter("Select Produse.Id_Produs, Produse.Nume_Produs, Produse.Pret From Produse Where Produse.Nume_Produs='" + (sender as Button).Name + "'", sqlConnection);
                sqlData.Fill(data);
                id = int.Parse(data.Rows[0][0].ToString());
                name = data.Rows[0][1].ToString();
                price = int.Parse(data.Rows[0][2].ToString());
                sqlConnection.Close();
            }
            if(ProdusInexist(id)!=1)
            {
                if (conn.State != ConnectionState.Open)
                {
                    conn.Open();
                }
                SqlCommand sqlCommand;
                String sql = "Insert into ListaComenzi(Id_Prod,Nume_Prod,Pret_Prod) Values(" + id + ",'" + name + "'," + price + ")";
                sqlCommand = new SqlCommand(sql, conn);
                sqlCommand.ExecuteNonQuery();

                conn.Close();
                MessageBox.Show("Comanda dumneavoastra a fost inregistrata!");
            }
            else
            {
                MessageBox.Show("Produsul a fost deja adaugat!");
            }
        }
        private int ProdusInexist(int ID)
        {
            using (SqlConnection sqlConnection = new SqlConnection(Properties.Settings.Default.MagazinFloriConnectionString))
            {
                DataTable data = new DataTable();
                data.Clear();
                if (sqlConnection.State != ConnectionState.Open)
                {
                    sqlConnection.Open();
                }
                SqlDataAdapter sqlData = new SqlDataAdapter("Select Count(*) From ListaComenzi Where Id_Prod="+ID+"", sqlConnection);
                sqlData.Fill(data);
                sqlConnection.Close();
                return int.Parse(data.Rows[0][0].ToString());   
            }
        }
    }
}
